function banAjax(id) {
    let element = event.currentTarget;
    let value = jQuery(element).is( ":checked" );
    jQuery.ajax({
        url: '/subscribers/ban',
        method: 'post',
        data: {
            id: id,
            value: +value
        },
        dataType: 'json',
        success: function (result) {
            if (result) {
                if (result.answer != true) {
                    let checkStatus = !element.checked;
                    jQuery(element).prop('checked', checkStatus);
                    jQuery(element).parent().find('.ui-switcher').attr('aria-checked', checkStatus);
                }
            }
        }
    });
}

function reportGenerate() {
    let value = jQuery('#subscriber').val();
    jQuery.ajax({
        url: '/report/process',
        method: 'post',
        data: { query: value },
        dataType: 'json',
        success: function (result) {
            if (result) {
                if (result.block) {
                    jQuery('#report-result').html(result.block);
                    jQuery('button.btn.btn-outline-success').removeClass('disabled');
                }
            }
        }
    });
}

function reportPrint()
{
    if (!jQuery(event.currentTarget).hasClass('disabled')) {
        jQuery("#report-result").print({
            stylesheet: '/css/library/bootstrap.min.css'
        });
    }
}