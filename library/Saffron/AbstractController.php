<?php

class Saffron_AbstractController extends Zend_Controller_Action
{
    public function preDispatch()
    {
        parent::preDispatch();
        $this->view->headLink()->appendStylesheet('/css/library/bootstrap.min.css');
        $this->view->headLink()->appendStylesheet('/css/library/fontawesome/all.min.css');
        $this->view->headLink()->appendStylesheet('/css/library/switcher.css');
        $this->view->headLink()->appendStylesheet('https://afeld.github.io/emoji-css/emoji.css');
        $this->view->headLink()->appendStylesheet('/css/main.css');
        $this->view->headScript()->appendFile('/js/library/jquery-3.3.1.min.js');
        $this->view->headScript()->appendFile('/js/library/bootstrap.min.js');
        $this->view->headScript()->appendFile('/js/library/jquery.switcher.min.js');
        $this->view->headScript()->appendFile('/js/library/jquery.mask.js');
        $this->view->headScript()->appendFile('/js/library/jQuery.print.js');
        $this->view->headScript()->appendFile('/js/main.js');
        $loginUrl = $this->getHelper('Url')->url(
            array(
                'controller' => 'account',
                'action' => 'login'
            )
        );
        $loginProcessUrl = $this->getHelper('Url')->url(
            array(
                'controller' => 'account',
                'action' => 'logining'
            )
        );
        if (!Zend_Auth::getInstance()->hasIdentity() && ($this->getRequest()->getRequestUri() != $loginUrl && $this->getRequest()->getRequestUri() != $loginProcessUrl)) {
            $this->redirect($loginUrl);
        } elseif (Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->menu = $this->view->partial('blocks/menu.phtml');
        }
        if (($this->_helper->flashMessenger->setNamespace('success') && $this->_helper->flashMessenger->hasMessages()) ||
            ($this->_helper->flashMessenger->setNamespace('error') && $this->_helper->flashMessenger->hasMessages())) {
            $this->view->messages = $this->_helper->flashMessenger->getMessages();
            $this->view->namespace = $this->_helper->flashMessenger->getNamespace();
        }
    }
}