<?php

class TariffsController extends Saffron_AbstractController
{
    public function indexAction()
    {
        $this->view->headTitle('Тарифи');

        $model = new Application_Model_Tariffs();
        $this->view->tariffs = $model->getAllTariffs();
    }

    public function addAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            unset($data['module'], $data['controller'], $data['action']);
            $model = new Application_Model_Tariffs();

            if ($model->save($data)) {
                $this->_helper->flashMessenger->setNamespace('success');
                $this->_helper->flashMessenger->addMessage('Збережено');
            } else {
                $this->_helper->flashMessenger->setNamespace('error');
                $this->_helper->flashMessenger->addMessage('Помилка');
            }

            $this->redirect('/tariffs/index');
        } else {
            $this->view->headTitle('Тарифи - Додати');
            $model = new Application_Model_Tariffs();
            $this->view->tariffs = $model->getAllTariffs();
        }
    }

    public function editAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            unset($data['module'], $data['controller'], $data['action'], $data['id']);
            $model = new Application_Model_Tariffs();

            if ($model->save($data)) {
                $this->_helper->flashMessenger->setNamespace('success');
                $this->_helper->flashMessenger->addMessage('Збережено');
            } else {
                $this->_helper->flashMessenger->setNamespace('error');
                $this->_helper->flashMessenger->addMessage('Помилка');
            }
            $this->redirect('/tariffs/index');
        } else {
            $this->view->headTitle('Тарифи - Редагувати');
            $id = $this->getRequest()->getParam('id');
            $this->getRequest()->clearParams();
            $model = new Application_Model_Tariffs();
            $this->view->tariff = $model->getTariffById($id);
        }
    }

    public function deleteAction()
    {
        $this->_helper->_layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $tariffId = $this->getRequest()->getParam('id');
        $model = new Application_Model_Tariffs();
        if ($model->deleteTariff($tariffId)) {
            $this->_helper->flashMessenger->setNamespace('success');
            $this->_helper->flashMessenger->addMessage('Видалено');
        } else {
            $this->_helper->flashMessenger->setNamespace('error');
            $this->_helper->flashMessenger->addMessage('Помилка');
        }

        $this->redirect('/tariffs/index');
    }
}