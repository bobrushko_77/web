<?php

class ReportController extends Saffron_AbstractController
{
    public function indexAction()
    {
        $model = new Application_Model_Subscribers();
        $this->view->subscribers = $model->getAllSubscribers();
    }

    public function processAction()
    {
        $this->_helper->_layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isXmlHttpRequest()) {
            $query = $this->getRequest()->getParam('query');
            $model = new Application_Model_Subscribers();
            $response = array();
            switch ($query) {
                case 'all':
                    $subscribers = $model->getAllSubscribers();
                    $response['block'] = $this->view->partial('report/table/all.phtml', array('subscribers' => $subscribers));
                    break;
                default:
                    $id = $this->getRequest()->getParam('subscriber');
                    $subscriber = $model->getSubscriberById($id);
                    $tariffModel = new Application_Model_Tariffs();
                    $tariff = $tariffModel->getTariffById($subscriber['tariff_id']);

                    $response['block'] = $this->view->partial('report/table/one.phtml', array('subscriber' => $subscriber, 'tariff' => $tariff));
                    break;
            }
            $this->getResponse()->appendBody(json_encode($response));
        }
    }
}