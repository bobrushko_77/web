<?php

class SubscribersController extends Saffron_AbstractController
{
	public function indexAction()
	{
		$this->view->headTitle('Користувачі');

		$model = new Application_Model_Subscribers();
		$this->view->subscribers = $model->getAllSubscribers();
	}

	public function addAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            unset($data['module'], $data['controller'], $data['action']);
            $model = new Application_Model_Subscribers();

            if ($model->save($data)) {
                $this->_helper->flashMessenger->setNamespace('success');
                $this->_helper->flashMessenger->addMessage('Успішно додано');
            } else {
                $this->_helper->flashMessenger->setNamespace('error');
                $this->_helper->flashMessenger->addMessage('Помилка');
            }

            $this->redirect('/subscribers/index');
        } else {
            $this->view->headTitle('Користувачі - Додати');
            $model = new Application_Model_Tariffs();
            $this->view->tariffs = $model->getAllTariffs();
        }
    }

    public function editAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            unset($data['module'], $data['controller'], $data['action'], $data['id']);
            $model = new Application_Model_Subscribers();

            if ($model->save($data)) {
                $this->_helper->flashMessenger->setNamespace('success');
                $this->_helper->flashMessenger->addMessage('Збережено');
            } else {
                $this->_helper->flashMessenger->setNamespace('error');
                $this->_helper->flashMessenger->addMessage('Помилка');
            }
            $this->redirect('/subscribers/index');
        } else {
            $this->view->headTitle('Користувачі - Редагувати');
            $id = $this->getRequest()->getParam('id');
            $this->getRequest()->clearParams();
            $model = new Application_Model_Subscribers();
            $this->view->subscriber = $model->getSubscriberById($id);
            $model = new Application_Model_Tariffs();
            $this->view->tariffs = $model->getAllTariffs();
        }
    }

	public function banAction()
    {
        $this->_helper->_layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isXmlHttpRequest()) {
            $data = array(
                'subscriber_id' => $this->getRequest()->getParam('id'),
                'banned' => $this->getRequest()->getParam('value')
            );
            $model = new Application_Model_Subscribers();
            $responce['answer'] = $model->save($data);

            $this->getResponse()->appendBody(json_encode($responce));
        }
    }

    public function deleteAction()
    {
        $this->_helper->_layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $subscriberId = $this->getRequest()->getParam('id');
        $this->getRequest()->clearParams();
        $model = new Application_Model_Subscribers();
        if ($model->deleteSubscriber($subscriberId)) {
            $this->_helper->flashMessenger->setNamespace('success');
            $this->_helper->flashMessenger->addMessage('Deleted');
        } else {
            $this->_helper->flashMessenger->setNamespace('error');
            $this->_helper->flashMessenger->addMessage('Помилка');
        }

        $this->redirect('/subscribers/index');
    }
}