<?php

class AccountController extends Saffron_AbstractController
{
    public function loginAction()
    {
        $this->view->headTitle('Вхід');
    }

    public function loginingAction()
    {
        $this->_helper->_layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isPost()) {
            $login = trim(strip_tags($this->getRequest()->getParam('login')));
            $password = md5('admin' . $this->getRequest()->getParam('password'));

            $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter(), 'users', 'login', 'password');
            $authAdapter->setIdentity($login)
                ->setCredential($password);
            $result = $authAdapter->authenticate();

            if ($result->isValid()) {
                $identity = $authAdapter->getResultRowObject(array('user_id', 'login'));
                Zend_Auth::getInstance()->getStorage()->write($identity);
            }

            $this->redirect(
                $this->getHelper('Url')->url(
                    array(
                        'controller' => 'index',
                        'action' => 'index'
                    )
                )
            );
        }
    }
}