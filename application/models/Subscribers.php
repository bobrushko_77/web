<?php

/**
 * Class Application_Model_Subscribers
 */
class Application_Model_Subscribers extends Zend_Db_Table_Abstract
{
    protected $_name = 'subscribers';
    protected $_subscriberId = 'subscriber_id';
    protected $_firstName = 'first_name';
    protected $_lastName = 'last_name';
    protected $_tariffId = 'tariff_id';
    protected $_balance = 'balance';
    protected $_banned = 'banned';

    /**
     * @return array
     */
    public function getAllSubscribers()
    {
        $subscribers = $this->getAdapter()->select()->from(array('subscribers' => $this->_name))->joinLeft(
            array('tar' => 'tariffs'), 'subscribers.tariff_id = tar.tariff_id', array('tariff' => 'name', 'speed' => 'speed', 'price' => 'price'))
            ->query()
            ->fetchAll();

        return $subscribers;
    }

    /**
     * @param $id
     * @return array
     */
    public function getSubscriberById($id)
    {
        $value = $this->fetchRow($this->_subscriberId . '=' . $id)->toArray();

        return $value;
    }

    /**
     * @param $data
     * @return bool
     */
    public function save($data)
    {
        $this->getAdapter()->beginTransaction();
        try {
            if ($data['subscriber_id']) {
                $id = $data['subscriber_id'];
                unset($data['subscriber_id']);
                $this->getAdapter()->update($this->_name, $data, $this->_subscriberId . '=' . $id);
            } else {
                $this->getAdapter()->insert($this->_name, $data);
            }
        }
        catch (Exception $e) {
            $this->getAdapter()->rollBack();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteSubscriber($id)
    {
        $this->getAdapter()->beginTransaction();
        try {
            $this->getAdapter()->delete($this->_name, $this->_subscriberId . '=' . $id);
        }
        catch (Exception $e) {
            $this->getAdapter()->rollBack();
            return false;
        }
        $this->getAdapter()->commit();
        return  true;
    }
}