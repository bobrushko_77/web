<?php

/**
 * Class Application_Model_Tariffs
 */
class Application_Model_Tariffs extends Zend_Db_Table_Abstract
{
    protected $_name = 'tariffs';
    protected $_tariffId = 'tariff_id';
    protected $_fieldName = 'name';
    protected $_speed = 'speed';
    protected $_price = 'price';

    /**
     * @return array
     */
    public function getAllTariffs()
    {
        $tarifs = $this->fetchAll()->toArray();

        return $tarifs;
    }

    /**
     * @param $id
     * @return array
     */
    public function getTariffById($id)
    {
        $value = $this->fetchRow($this->_tariffId . '=' . $id)->toArray();

        return $value;
    }

    /**
     * @param $data
     * @return bool
     */
    public function save($data)
    {
        $this->getAdapter()->beginTransaction();
        try {
            if ($data['tariff_id']) {
                $id = $data['tariff_id'];
                unset($data['tariff_id']);
                $this->getAdapter()->update($this->_name, $data, $this->_tariffId . '=' . $id);
            } else {
                $this->getAdapter()->insert($this->_name, $data);
            }
        }
        catch (Exception $e) {
            $this->getAdapter()->rollBack();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteTariff($id)
    {
        $this->getAdapter()->beginTransaction();
        try {
            $this->getAdapter()->delete($this->_name, $this->_tariffId . '=' . $id);
        }
        catch (Exception $e) {
            $this->getAdapter()->rollBack();
            return false;
        }
        $this->getAdapter()->commit();
        return  true;
    }
}