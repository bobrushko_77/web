-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июн 03 2019 г., 20:11
-- Версия сервера: 10.1.38-MariaDB
-- Версия PHP: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `billing-zend`
--
CREATE DATABASE IF NOT EXISTS `billing-zend` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `billing-zend`;

-- --------------------------------------------------------

--
-- Структура таблицы `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE `subscribers` (
  `subscriber_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tariff_id` int(11) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `banned` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscribers`
--

INSERT INTO `subscribers` (`subscriber_id`, `first_name`, `last_name`, `phone`, `email`, `tariff_id`, `address`, `balance`, `banned`) VALUES
(1, 'BÑÐµÐ²Ð¾Ð»Ð¾Ð´', 'Ð¯Ð½Ð¾Ð²Ð¸Ñ‡', '+38 (096) 007-23-95', 'tbodnarenko@zavodzet.ru', 7, 'Ð—Ð°Ð¿Ð¾Ñ€Ñ–Ð·ÑŒÐºÐ° Ð¾Ð±Ð»Ð°ÑÑ‚ÑŒ, Ð¼Ñ–ÑÑ‚Ð¾ Ð—Ð°Ð¿Ð¾Ñ€Ñ–Ð¶Ð¶Ñ, Ð¿Ñ€Ð¾Ð². Ð’Ð¾Ð»ÐºÐ¾Ð²Ð°, 27', '0.00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tariffs`
--

DROP TABLE IF EXISTS `tariffs`;
CREATE TABLE `tariffs` (
  `tariff_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `speed` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tariffs`
--

INSERT INTO `tariffs` (`tariff_id`, `name`, `speed`, `price`) VALUES
(7, 'Ð‘Ð°Ð·Ð¾Ð²Ð¸Ð¹', 100, '95.00');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`) VALUES
(1, 'admin', 'f6fdffe48c908deb0f4c3bd36c032e72');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`subscriber_id`),
  ADD KEY `tariff` (`tariff_id`);

--
-- Индексы таблицы `tariffs`
--
ALTER TABLE `tariffs`
  ADD PRIMARY KEY (`tariff_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `subscriber_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tariffs`
--
ALTER TABLE `tariffs`
  MODIFY `tariff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `subscribers`
--
ALTER TABLE `subscribers`
  ADD CONSTRAINT `tariff` FOREIGN KEY (`tariff_id`) REFERENCES `tariffs` (`tariff_id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
